package com.gitlab.ci;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by Administrator on 2017/5/29 0029.
 */
@SpringBootApplication
public class GitlabCIApplication {
    public static void main(String[] args) {
        SpringApplication.run(GitlabCIApplication.class, args);
    }
}
